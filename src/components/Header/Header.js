import React from "react";
import "./Header.css";
import Img from "./logo.png";
import SearchIcon from "@material-ui/icons/Search";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";

function Header() {
  return (
    <div className="header">
      <Link to="/">
        <img className="header_logo" src={Img} alt="" />
      </Link>
      <div className="header_topics">
        <Tabs className="tabs-style">
          <nav>
            <NavLink exact activeClassName="active" to="/">
              <Tab indicatorColor="primary" textColor="primary" label="Home" />
            </NavLink>
            <NavLink activeClassName="active" to="/Development">
              <Tab
                indicatorColor="primary"
                textColor="primary"
                label="Development"
              />
            </NavLink>
            <NavLink activeClassName="active" to="/Systems">
              <Tab
                indicatorColor="primary"
                textColor="primary"
                label="Systems"
              />
            </NavLink>
            <NavLink activeClassName="active" to="/Extra-Curricular">
              <Tab
                indicatorColor="primary"
                textColor="primary"
                label="Extra-Curricular"
              />
            </NavLink>
          </nav>
        </Tabs>
        {/* Links for different blog topics */}
      </div>
      <div className="header_search">
        <SearchIcon />
        <input placeholder="Search GingerITPro" />
      </div>
    </div>
  );
}

export default Header;
