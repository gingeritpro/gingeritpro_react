import React from "react";
import "./App.css";
import Header from "./components/Header/Header.js";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
        <div className="app__body">
          <Switch>
            <Route path="/blog/:category"></Route>
            <Route path="/">
              <h1>Place Holder Homepage :)</h1>
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
